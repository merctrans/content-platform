export interface IUser {
  email: string;
  password: string;
  contentType: string;
  phone: string;
  country: string;
  city: string;
}

export interface IWriter extends IUser {
  firstName: string;
  lastName: string;
}

export interface ICompany extends IUser{
  name: string;
  address: string;
  taxID: string;
}
