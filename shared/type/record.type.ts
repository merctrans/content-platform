export interface IDBRecord {
  _id: string;
  createdAt?: Date;
  updatedAt?: Date;
}
