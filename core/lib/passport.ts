import passport from 'passport';
import local, {VerifyFunction} from 'passport-local';

/**
 * Description: Verify local strategy
 * @param username
 * @param password
 * @param done
 */
const localVerify: VerifyFunction = (username, password, done) => {

}

passport.use(new local.Strategy(localVerify))
