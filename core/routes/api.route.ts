import { Router } from 'express';
import writerRouter from '@routes/writer.route';
import companyRouter from '@routes/company.route';

const apiRouter = Router();

apiRouter.use('/writer', writerRouter);
apiRouter.use('/company', companyRouter);

export default apiRouter;
