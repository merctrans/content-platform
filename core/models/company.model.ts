import mongoose, { Document, Schema, Model } from 'mongoose';
import { IWriter } from '@type/user.type';

export interface IWriterDoc extends Document, IWriter {}

export interface IWriterModel extends Model<IWriterDoc> {}

const WriterSchema: Schema<IWriterDoc, IWriterModel> = new Schema({
  email: String,
  password: String,
  contentType: String,
  phone: String,
  country: String,
  city: String,

  // Note: info of company
  name: String,
  address: String,
  taxID: String,
});

const WriterModel = mongoose.model<IWriterDoc, IWriterModel>('Writer', WriterSchema);

export default WriterModel;
