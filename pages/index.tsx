import React from 'react';
import Home from '@views/Home/Home';

interface OwnProps {}

type Props = OwnProps;

const index: React.FC<Props> = (props: Props) => {
  return <Home />;
};

export default index;
