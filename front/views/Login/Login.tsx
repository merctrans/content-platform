import React from 'react';
import loginStyles from '@styles/Login.module.css';
import { Input, Button, Checkbox, Form } from 'antd';
import Link from 'next/link';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

interface OwnProps {}

type Props = OwnProps;

const Login: React.FC<Props> = (props: Props) => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };
  return (
    <div className={loginStyles.root}>
      <Form {...layout} name="basic" onFinish={onFinish}>
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, type: 'email', message: 'The input is not valid E-mail!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>
        <div className={loginStyles.loginButtonWrapper}>
          <Button shape="round" type="primary" htmlType="submit">
            Login
          </Button>
          <Link href="/register">
            <Button shape="round" type="primary" className={loginStyles.registerWrapper}>
              Register
            </Button>
          </Link>
        </div>
      </Form>
    </div>
  );
};

export default Login;
