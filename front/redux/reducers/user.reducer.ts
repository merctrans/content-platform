import { Reducer } from 'redux';

export type UserState = {
  name: string;
};
const initial: UserState = {
  name: 'Tuan',
};

const userReducer: Reducer<UserState> = (state = initial, action) => {
  return state;
};

export default userReducer;
